<?php

/**
 * @file
 * Weather block template.
 *
 * Available variables:
 *   $temperature
 *   $feels_like
 *   $precipitation.
 */
?>
<div class="weather-box">
  <h2 class="black">Waterloo Weather</h2>
  <div class="weather-content">
    <h3><a href="http://weather.uwaterloo.ca/">Waterloo Weather Station</a></h3>

    <div class="weather-data">
      <div class="weather-icon temperature">Temperature</div>
      <div class="weather-reading">
        <?php print ($temperature); ?> &deg;C
        <?php if (!empty($feels_like)): ?>
          <br /><span class="feels-like">Feels like <?php print ($feels_like); ?> &deg;C</span>
        <?php endif; ?>
      </div>
    </div>

    <div class="weather-data">
      <div class="weather-icon precipitation">Precipitation</div>
      <div class="weather-reading"><?php print ($precipitation); ?> mm</div>
    </div>

    <?php if ($time) : ?>
    <div class="weather-time">Current readings as of <?php print ($time); ?></div>
    <?php endif; ?>

  </div>

  <p class="morelink weather-sources"><a href="/weather">See more weather sources &raquo;</a></p>
  <p class="morelink weather-station"><a href="http://weather.uwaterloo.ca/">View current readings &raquo;</a></p>
</div>
